name := "management-play26"

resolvers += "uxforms-public" at "https://artifacts-public.uxforms.net"

publishTo := Some("uxforms-public-management" at "s3://artifacts-public.uxforms.net")

libraryDependencies ++= Seq(
    "com.gu" %% "management" % "5.47",
    "com.gu" %% "management-internal" % "5.47",
    "com.gu" %% "management-logback" % "5.47",
    "com.typesafe.play" %% "play" % "2.6.1",
    "com.typesafe.play" %% "play-test" % "2.6.1" % "test",
    "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % "test",
    guice
)

// needed for Play
resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false

